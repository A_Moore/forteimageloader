package by.blaaster.test.forteimageloader.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import by.blaaster.test.forteimageloader.R;
import by.blaaster.test.forteimageloader.fragment.ImageHolderFragment;

public class ImageDisplayActivity extends AppCompatActivity {
    public static String EXTRA_IMAGE_URL = "image_url";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_display);

        ImageHolderFragment fragment = ImageHolderFragment.getRetainFragment(getSupportFragmentManager());

        fragment.assignBitmap(getIntent().getStringExtra(EXTRA_IMAGE_URL), R.id.image);
    }
}
