package by.blaaster.test.forteimageloader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Mozhugov on 20.04.2016.
 */
public class Constants {
    public static List<String> imageUrlList;

    static {
        imageUrlList = new ArrayList<>(2);
        imageUrlList.add("http://images5.fanpop.com/image/photos/27900000/Ocean-Animals-animals-27960311-1920-1200.jpg");
        imageUrlList.add("http://www.heartofgreen.org/.a/6a00d83451cedf69e201a73dcaba0a970d-pi");
    }
}
