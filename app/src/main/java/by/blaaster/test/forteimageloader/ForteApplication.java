package by.blaaster.test.forteimageloader;

import android.app.Application;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

/**
 * Created by Alexander Mozhugov on 19.04.2016.
 */
public class ForteApplication extends Application {
    private static LruCache<String, Bitmap> mLruCache;
    private static ForteApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();

        final int cacheSize = (int) (Runtime.getRuntime().maxMemory() / 1024) / 2;

        mLruCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }
        };

        instance = this;
    }

    public static void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mLruCache.put(key, bitmap);
        }
    }

    public static Bitmap getBitmapFromMemCache(String key) {
        return mLruCache.get(key);
    }

    public static ForteApplication getInstance() {
        return instance;
    }
}
