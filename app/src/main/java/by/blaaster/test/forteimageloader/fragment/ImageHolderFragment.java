package by.blaaster.test.forteimageloader.fragment;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.ImageView;

import by.blaaster.test.forteimageloader.network.DownloadImageTask;

/**
 * Created by Alexander Mozhugov on 19.04.2016.
 */
public class ImageHolderFragment extends Fragment {
    private static final String TAG = "test";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    public void assignBitmap(String url, int imageViewId) {
        new DownloadImageTask(this, imageViewId).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, url);
    }

    public void onBitmapLoaded(Bitmap bitmap, int imageViewId) {

        ImageView imageView = (ImageView) getActivity().findViewById(imageViewId);
        imageView.setImageBitmap(bitmap);
    }

    public static ImageHolderFragment getRetainFragment(FragmentManager fragmentManager) {
        ImageHolderFragment fragment = (ImageHolderFragment) fragmentManager.findFragmentByTag(TAG);
        if (fragment == null) {
            fragment = new ImageHolderFragment();
            fragmentManager.beginTransaction().add(fragment, TAG).commit();
        }
        return fragment;
    }
}
