package by.blaaster.test.forteimageloader.network;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import by.blaaster.test.forteimageloader.ForteApplication;
import by.blaaster.test.forteimageloader.fragment.ImageHolderFragment;

/**
 * Created by Alexander Mozhugov on 19.04.2016.
 */
public class DownloadImageTask extends AsyncTask<String, Void, Void> {
    private static final String TAG = "test";
    private static final int CONNECTION_TIMEOUT = 20000;
    private static final int READ_TIMEOUT = 15000;

    private ImageHolderFragment mFragment;
    private Bitmap mBitmap;
    private int mImageViewId;

    public DownloadImageTask(ImageHolderFragment fragment, int imageViewId) {
        mFragment = fragment;
        mImageViewId = imageViewId;
    }

    @Override
    protected Void doInBackground(String... params) {
        try {
            String url = params[0];
            mBitmap = ForteApplication.getInstance().getBitmapFromMemCache(url);
            if (mBitmap != null) {
                return null;
                //TODO implement disk cache
            } else {
                Log.d(TAG, "Image can not be retrieved from memory cache");
                URL imageUrl = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) imageUrl.openConnection();
                connection.setConnectTimeout(CONNECTION_TIMEOUT);
                connection.setReadTimeout(READ_TIMEOUT);
                connection.setInstanceFollowRedirects(true);
                InputStream is = connection.getInputStream();
                mBitmap = BitmapFactory.decodeStream(is);
                ForteApplication.getInstance().addBitmapToMemoryCache(url, mBitmap);
            }
        } catch (MalformedURLException e) {
            Log.d(TAG, "wrong URL");
        } catch (IOException e) {
            Log.d(TAG, "Cant download image");
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        mFragment.onBitmapLoaded(mBitmap, mImageViewId);
    }
}
