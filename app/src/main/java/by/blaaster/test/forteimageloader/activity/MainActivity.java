package by.blaaster.test.forteimageloader.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import by.blaaster.test.forteimageloader.Constants;
import by.blaaster.test.forteimageloader.R;
import by.blaaster.test.forteimageloader.fragment.ImageHolderFragment;
import by.blaaster.test.forteimageloader.view.CheckableImageView;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "test.forte";
    private static final int PADDING_IMAGE = 20;
    private static final String PREFS_SELECTION = "selection";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageHolderFragment fragment = ImageHolderFragment.getRetainFragment(getSupportFragmentManager());
        LinearLayout container = (LinearLayout) findViewById(R.id.images_container);

        String savedUrl = getPreferences(MODE_PRIVATE).getString(PREFS_SELECTION, "");
        int i = 0;
        for (String url : Constants.imageUrlList) {
            CheckableImageView imageView = new CheckableImageView(this, url);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            imageView.setId(i);
            imageView.setAdjustViewBounds(true);
            imageView.setPadding(PADDING_IMAGE, PADDING_IMAGE, PADDING_IMAGE, PADDING_IMAGE);
            container.addView(imageView);
            if (savedUrl.equals(url)) {
                imageView.setChecked(true);
            } else {
                imageView.setChecked(false);
            }
            fragment.assignBitmap(url, imageView.getId());

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LinearLayout container = (LinearLayout) findViewById(R.id.images_container);
                    for (int i = 0; i < container.getChildCount(); i++) {
                        ((CheckableImageView) container.getChildAt(i)).setChecked(false);
                    }
                    ((CheckableImageView) v).setChecked(true);
                    getPreferences(MODE_PRIVATE).edit().putString(PREFS_SELECTION, ((CheckableImageView) v).getUrl()).commit();
                }
            });
            i++;
        }

        //noinspection ConstantConditions
        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout container = (LinearLayout) findViewById(R.id.images_container);
                for (int i = 0; i < container.getChildCount(); i++) {
                    if (((CheckableImageView) container.getChildAt(i)).isChecked()) {
                        String url = ((CheckableImageView) container.getChildAt(i)).getUrl();
                        Intent intent = new Intent(getApplicationContext(), ImageDisplayActivity.class);
                        intent.putExtra(ImageDisplayActivity.EXTRA_IMAGE_URL, url);
                        startActivity(intent);
                    }
                }
            }
        });
    }
}
