package by.blaaster.test.forteimageloader.view;

import android.content.Context;
import android.graphics.Color;
import android.widget.Checkable;
import android.widget.ImageView;

/**
 * Created by Alexander Mozhugov on 20.04.2016.
 */
public class CheckableImageView extends ImageView implements Checkable {
    private boolean mChecked;
    private String mUrl;

    public CheckableImageView(Context context, String url) {
        super(context);
        mUrl = url;
    }

    @Override
    public void toggle() {
        setChecked(!mChecked);
        upgradeBackground();
    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void setChecked(final boolean checked) {
        if (mChecked == checked)
            return;
        mChecked = checked;
        upgradeBackground();
    }

    private void upgradeBackground() {
        if (isChecked()) {
            setBackgroundColor(Color.BLACK);
        } else {
            setBackgroundColor(Color.WHITE);
        }
    }

    public String getUrl() {
        return mUrl;
    }
}